Heuristic: Edit distance

Code: /home/tanuma/Project-Knowledge Base/Implementation/edit_distance/ed.py
Author: Tanuma
Purpose: Resolves the entities based on edit distance between the entity names
Input: all entity list
Entity from: webopedia "/mnt/dell/Ashwini/edit_distance/Tags_Webopedia.txt"
	     techtarget "/mnt/dell/Ashwini/edit_distance/Tags_Techtarget.txt"
	     and from knowledgeBase 
Output: refined entity list
