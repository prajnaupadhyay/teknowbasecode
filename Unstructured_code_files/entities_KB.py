import re
import sys
from nltk.corpus import stopwords

stop = set(stopwords.words('english'))

file = 'latestkb.csv'
handle = open(file,'r')
entities = handle.readlines()
handle.close()

output = set()
for e in entities:
	ent = e.split(',')
	if ent[0] not in stop and not ent[0].isdigit():
		ent[0] = ent[0].replace('_',' ')
		output.add(ent[0])
	if ent[2] not in stop and not ent[2].isdigit():
		ent[2] = ent[2].replace('_',' ')
		output.add(ent[2])

list = list(output)

file = 'KB_entities_Updated.txt'
handle = open(file,'w')

for l in list:
	handle.write(l)
	handle.write('\n')
