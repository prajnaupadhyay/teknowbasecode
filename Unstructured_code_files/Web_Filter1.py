from collections import deque
import re
import sys
import nltk
reload(sys)  
sys.setdefaultencoding('utf8')

AdjList = []
 
def init_trie(keywords):
	""" creates a trie of keywords, then sets fail transitions """
	create_empty_trie()
	add_keywords(keywords)
	set_fail_transitions()
 
def create_empty_trie():
	""" initalize the root of the trie """
	AdjList.append({'value':'', 'next_states':[],'fail_state':0,'output':[]})
 
def add_keywords(keywords):
	""" add all keywords in list of keywords """
	for keyword in keywords:
		add_keyword(keyword)

def find_next_state(current_state, value):
	for node in AdjList[current_state]["next_states"]:
		if AdjList[node]["value"] == value:
			return node
	return None

def add_keyword(keyword):
	""" add a keyword to the trie and mark output at the last node """
	current_state = 0
	j = 0
	keyword = keyword.lower()
	child = find_next_state(current_state, keyword[j])
	while child != None:
		current_state = child
		j = j + 1
		if j < len(keyword):
			child = find_next_state(current_state, keyword[j])
		else:
			break
	for i in range(j, len(keyword)):
		node = {'value':keyword[i],'next_states':[],'fail_state':0,'output':[]}
		AdjList.append(node)
		AdjList[current_state]["next_states"].append(len(AdjList) - 1)
		current_state = len(AdjList) - 1
	AdjList[current_state]["output"].append(keyword)	

def set_fail_transitions():
	q = deque()
	child = 0
	for node in AdjList[0]["next_states"]:
		q.append(node)
		AdjList[node]["fail_state"] = 0
	while q:
		r = q.popleft()
		for child in AdjList[r]["next_states"]:
			q.append(child)
			state = AdjList[r]["fail_state"]
		while find_next_state(state, AdjList[child]["value"]) == None and state != 0:
				state = AdjList[state]["fail_state"]
		AdjList[child]["fail_state"] = find_next_state(state, AdjList[child]["value"])
		if AdjList[child]["fail_state"] is None:
			AdjList[child]["fail_state"] = 0
		AdjList[child]["output"] = AdjList[child]["output"] + AdjList[AdjList[child]["fail_state"]]["output"]	
	
def get_keywords_found(line):
	""" returns true if line contains any keywords in trie """
	line = line.lower()
	current_state = 0
	keywords_found = []
 
	for i in range(len(line)):
		while find_next_state(current_state, line[i]) is None and current_state != 0:
			current_state = AdjList[current_state]["fail_state"]
		current_state = find_next_state(current_state, line[i])
		if current_state is None:
			current_state = 0
		else:
			for j in AdjList[current_state]["output"]:
				keywords_found.append(j)
	keywords_found.sort(key=len,reverse=True)
	return keywords_found	
	
file = 'KB_entities.txt' #Entity list constructed from the entities in the TKB
handle = open(file,'r')
entities = handle.readlines()
handle.close()

entities2 = []
for e in entities:
	e = e.replace('\n','')
	e = e.replace('\n','')
	e = e.replace('_','')
	e = e.lower()
	e = ' '+e+' '
	entities2.append(e)

entities2.sort(key=len, reverse=True)

init_trie(entities2)

file = 'web_OI.txt'
handle = open(file,'r')
lines = handle.readlines()
handle.close()

li =  []
for l in range(len(lines)):
	if(lines[l]=='\n'):
		li.append(l)

values = []
index = []
i = 0

for l in range(len(lines)):
	lines[l] = lines[l].replace('\n','')
	lines[l] = lines[l].replace('\r','')
	lines[l] = lines[l].lower()	
	if(lines[l].startswith('0.')):
		#print(l[7:])
		values.insert(i,lines[l][6:])
		index.insert(i,0)
		#handle.write(lines[l][6:])
		i=i+1
		
file = 'Web_Filter1.txt'
handle = open(file,'w')

def string_found(string1,string2):
	if re.search(r"\b"+re.escape(string1) +r"\b",string2):
		return True
	return False	

def getIndex(s1,s2):
	e1 = nltk.word_tokenize(s1)
	e2 = nltk.word_tokenize(s2)
	i = 0
	for j in range(len(e1)):
		if(e1[j]==e2[0]):
			i = j
	k = i-1
	if(e1[k]=='the'):
		#print(1)
		return True
	return False


triple_dict = {}
z = 0
for v in range(len(values)):	
	if(values[v]!='' and index[v]==0):		
		ent = values[v].split(';')
		if(len(ent)==3 and ent[2]!='' and ent[0]!='' and ent[1]!=''):
			f1 = ent[0]
			ent[2] = ent[2].split('[')[0]
			f2 = ent[2]
			ent[0] = ent[0].replace('(',' ')
			ent[2] = ent[2].replace(')',' ')
			ent[0] = ent[0].replace(' an ',' ')
			ent[2] = ent[2].replace(' an ',' ')
			ent[0] = ent[0].replace(' a ',' ')
			ent[2] = ent[2].replace(' a ',' ')
			ent[0] = ent[0].replace(' the ',' ')
			ent[2] = ent[2].replace(' the ',' ')
			ent[0] = ent[0].replace(' in ',' ')
			ent[2] = ent[2].replace(' in ',' ')
			ent[0] = ent[0].replace(' if ',' ')
			ent[2] = ent[2].replace(' if ',' ')
			ent[0] = ent[0].replace(' of ',' ')
			ent[2] = ent[2].replace(' of ',' ')
			ent[0] = ent[0].replace(',','')
			ent[2] = ent[2].replace(',','')
			ent[0] = ent[0].strip()
			ent[2] = ent[2].strip()
			ent[0] = ' '+ent[0]+' '
			ent[2] = ' '+ent[2]+' '	
			if(get_keywords_found(str(ent[0]))!=[] and index[v]== 0):
				if(get_keywords_found((ent[2]))!=[] and get_keywords_found(ent[0])!=get_keywords_found(ent[2]) and index[v]==0):
					if(values[v] not in triple_dict.values()):
						index[v] = 1
						key = z
						z = z+1
						triple_dict[key] = values[v]
						e1 = nltk.word_tokenize(ent[0])
						e2 = nltk.word_tokenize(ent[2])
						r1 = get_keywords_found(ent[0])[0]
						r2 = get_keywords_found(ent[2])[0]
						r3 = nltk.word_tokenize(r1)
						r4 = nltk.word_tokenize(r2)

						per1 = ((len(r3))/(len(e1)))*100
						per2 = ((len(r4))/(len(e2)))*100
						#if(per1>=50 and per2>=50):	
								
						#handle.write(str(my_dict.get(str(values[v]))))
						#handle.write('\t')
						handle.write(str(values[v]))
						handle.write('\t')
						handle.write(ent[1]) #Prints the relation part of the triple.
						handle.write('\t')
						handle.write(str(get_keywords_found(ent[0])[0]))
						handle.write('\t')
						handle.write(str(get_keywords_found(ent[2])[0]))
						handle.write('\t')
						#a = my_dict.get(values[v])
						#print(a)
						#b = dict.get(a.strip())
						#print(b)
						#s = '/wikipedia/first_section/' +b
						handle.write('Webopedia')
						handle.write('\n')	
handle.close()