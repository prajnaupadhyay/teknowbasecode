import re
import nltk
import sys
from collections import deque

AdjList = []
 
def init_trie(keywords):
	""" creates a trie of keywords, then sets fail transitions """
	create_empty_trie()
	add_keywords(keywords)
	set_fail_transitions()
 
def create_empty_trie():
	""" initalize the root of the trie """
	AdjList.append({'value':'', 'next_states':[],'fail_state':0,'output':[]})
 
def add_keywords(keywords):
	""" add all keywords in list of keywords """
	for keyword in keywords:
		add_keyword(keyword)

def find_next_state(current_state, value):
	for node in AdjList[current_state]["next_states"]:
		if AdjList[node]["value"] == value:
			return node
	return None

def add_keyword(keyword):
	""" add a keyword to the trie and mark output at the last node """
	current_state = 0
	j = 0
	keyword = keyword.lower()
	child = find_next_state(current_state, keyword[j])
	while child != None:
		current_state = child
		j = j + 1
		if j < len(keyword):
			child = find_next_state(current_state, keyword[j])
		else:
			break
	for i in range(j, len(keyword)):
		node = {'value':keyword[i],'next_states':[],'fail_state':0,'output':[]}
		AdjList.append(node)
		AdjList[current_state]["next_states"].append(len(AdjList) - 1)
		current_state = len(AdjList) - 1
	AdjList[current_state]["output"].append(keyword)	

def set_fail_transitions():
	q = deque()
	child = 0
	for node in AdjList[0]["next_states"]:
		q.append(node)
		AdjList[node]["fail_state"] = 0
	while q:
		r = q.popleft()
		for child in AdjList[r]["next_states"]:
			q.append(child)
			state = AdjList[r]["fail_state"]
		while find_next_state(state, AdjList[child]["value"]) == None and state != 0:
				state = AdjList[state]["fail_state"]
		AdjList[child]["fail_state"] = find_next_state(state, AdjList[child]["value"])
		if AdjList[child]["fail_state"] is None:
			AdjList[child]["fail_state"] = 0
		AdjList[child]["output"] = AdjList[child]["output"] + AdjList[AdjList[child]["fail_state"]]["output"]	
	
def get_keywords_found(line):
	""" returns true if line contains any keywords in trie """
	line = line.lower()
	current_state = 0
	keywords_found = []
 
	for i in range(len(line)):
		while find_next_state(current_state, line[i]) is None and current_state != 0:
			current_state = AdjList[current_state]["fail_state"]
		current_state = find_next_state(current_state, line[i])
		if current_state is None:
			current_state = 0
		else:
			for j in AdjList[current_state]["output"]:
				keywords_found.append(j)
	keywords_found.sort(key=len,reverse=True)
	return keywords_found


file = 'TT_Filter3_Updated.txt'
handle = open(file,'r')
lines = handle.readlines()
handle.close()

file = 'Normal_formRelations_Updated.txt'
handle = open(file,'r')
entities = handle.readlines()
handle.close()

def between_pos_tags(s):
	try:
		list = []
		token = nltk.word_tokenize(s)
		pos = nltk.pos_tag(token)
		#handle.write(str(pos))
		#handle.write('\n')
		for l in pos:
			if(l[1]=="NN" or l[1]=="NNP" or l[1]=="NNS" or l[1]=="NNPS"):
				list.append(l[0])
		return list		
	except ValueError:
		return ""

file = 'NounsrelationsTT50per_Updated.txt'
handle = open(file,'w')

noun_list = []
for e in entities:
	e = e.replace('_',' ')
	s1 = between_pos_tags(e)
	s1 = ' '.join(s1)
	if(s1!=''):
		s2 = ' '+s1+' '
		noun_list.append(s2)	
#print(noun_list)
init_trie(noun_list)

for l in lines:
	ent = l.split('\t')
	first_part = between_pos_tags(ent[1])
	
	first_part = ' '.join(first_part)

	first_part = ' '+first_part+' '
	if(first_part!=''):
		#print(first_part)
		if(get_keywords_found(first_part)!=[]):
			r1 = nltk.word_tokenize(first_part)
			r2 = nltk.word_tokenize(str(get_keywords_found(first_part)[0]))
			per1 = (len(r1)*100)/(len(r2))
			if(per1>=50):
				#print(get_keywords_found(first_part),',',first_part)
				handle.write(ent[0])
				handle.write('\t')
				handle.write(first_part)
				handle.write('\t')
				handle.write(ent[2])
				handle.write('\t')
				handle.write(ent[3])
				handle.write('\t')
				handle.write(ent[4])
	
handle.close()