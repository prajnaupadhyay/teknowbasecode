import re
import sys

file = 'Relation_Updated.txt'
handle = open(file,'r')
entities = handle.readlines()
handle.close()

def convert(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

file = 'Normal_formRelations_Updated.txt'
handle = open(file,'w')

for e in entities:
	e = e.replace('\n','')
	s = convert(e)
	handle.write(s)
	handle.write('\n')
handle.close()