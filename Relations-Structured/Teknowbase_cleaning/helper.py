
fileName = 'latestkb.csv'
import csv
import urllib2 
from bs4 import BeautifulSoup
import string
import unicodedata

#divide triples as new,retained,discarded
newFile = 'list_hierarchy_new_collection.txt'
retainFile = 'list_hierarchy_retained.txt'
rubbish = 'lh_discarded.txt'
targetnew = open(newFile, 'w')
targetret = open(retainFile,'w')
throw = open(rubbish,'w')

#read all the lines in kb
csvfile = open(fileName)
readerFile = csv.reader(csvfile)
data = list(readerFile)
main_count = len(data)
print main_count

#Some counts to know stats
del_count = 0 
retained = 0 
base_url = 'https://en.wikipedia.org'

#filter these headings out
unwanted = ['Contents','See also','References','External links','Footnotes','Further reading']

#lists for some checks
source_list = []
fresh_triples = []

#Utility Functions
def cleanSourceString(source):
	source = source.replace('\n','')
	source = source.replace('\\',' ')
	source = source.replace(' n','')
	return source
	
def getContent(main_url):
	print '\n==================',main_url,'====================\n'
	page = ''
	try:
		page = urllib2.urlopen(main_url)
	except:
		print 'not found'	
	soup = BeautifulSoup(page,"lxml")
	condiv = soup.findAll('div', id="mw-content-text")
	return condiv
	
def validateTypeOf(condiv, triple):
	global targetret
	global throw
	print 'validating typeOf'
	allh2 = condiv[0].findAll('h2')	
	e1 = triple[0]
	el1 = ' '.join(e1.split('_'))
	for heading in allh2:
		curr = heading.next_sibling
		while (curr and curr.name != 'ul') and (curr and curr.name != 'h2'):
			if curr.name == 'ul' or curr.name == 'dl' or curr.name == 'h3':
				break
			curr = curr.next_sibling
			if curr and curr.name == 'ul' and triple[1].find('typeOf') != -1:							
				ulist1 = curr.contents							
				ulist1_ref =filter(lambda a: a != u'\n', ulist1)
				#print ulist1_ref
				flag = False
				for ele in ulist1_ref:
					hyperlink = ele.find('a')		
					if hyperlink:
						k = hyperlink.text
						htext = unicodedata.normalize('NFKD', k).encode('ascii','ignore')
						print htext.lower(),' e1 ',el1																	
						if htext.lower() == el1:
							print 'entity matched: ',htext,' ',el1
							print 'valid typeOf: ',triple
							targetret.write(triple)
							targetret.write('\n')
						else:
							print 'discarded typeOf: ',triple
							throw.writ(triple)
							throw.write('\n')
							
	
def correctEntity(heading):
	hyperlink = heading.find('a')
	plain = heading.text
	normal_entity = unicodedata.normalize('NFKD', plain).encode('ascii','ignore')		
	normal_entity = normal_entity.replace('[edit]','')
	if hyperlink:
		if hyperlink.text == 'edit':
			nsib = heading.find_next_sibling('div')
			if nsib and	nsib.text.find('Main article') != -1:
				k = nsib.find('a').text
				entity1 = unicodedata.normalize('NFKD', k).encode('ascii','ignore')
				return entity1
			else:				
				return normal_entity
	return normal_entity		
			
			
def addFreshHeadingRelations(condiv,triple,source):
	global fresh_triples
	global targetnew
	allh2 = condiv[0].findAll('h2')	
	for heading in allh2:
		entity = correctEntity(heading)
		if entity not in unwanted:
			entity = '_'.join(entity.split(' ')).lower()
			nt = entity+'\tisTerminology(relatedTo)\t'+triple[2]+'\t'+source	
			fresh_triples.append(nt)
			print 'fresh : \t',nt
			targetnew.write(nt)
			targetnew.write('\n')	
							
		
def filterTriple(triple,url):
	global fresh_triples
	global throw
	print 'filtering:\t',triple
	triple = '\t'.join(triple)
	if triple not in fresh_triples:
		print 'discarded:\t',triple
		throw.write(triple)
		throw.write('\n')
	

		
							
							
			
	
		

	
