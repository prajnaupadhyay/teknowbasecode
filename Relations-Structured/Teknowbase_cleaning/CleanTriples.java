/*
 * Author: Prajna Upadhyay
 * 
 * stems the entities and removes duplicate entries
 * fixes a number of other issues in the kb*/

package preprocess;

import java.io.*;
import java.util.*;

import buildAndProcessTrees.BuildTree;
import buildAndProcessTrees.GetPropertyValues;
//import buildAndProcessTrees.TreeNode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import org.apache.commons.lang3.math.NumberUtils;

public class CleanTriples
{
	public static void cleanTriples(BufferedReader br1, HashMap<String, String> hm, String s1, String s2, String s3, String s4, HashMap<String, String> yagoTypes) throws Exception
	{
		
		BufferedWriter bw1 = new BufferedWriter(new FileWriter(s1));
		BufferedWriter bw2 = new BufferedWriter(new FileWriter(s2));
		BufferedWriter bw3 = new BufferedWriter(new FileWriter(s3));
		BufferedWriter bw4 = new BufferedWriter(new FileWriter(s4));
		HashMap<String, ArrayList<Edge>> types = new HashMap<String, ArrayList<Edge>>();
		HashMap<String, HashMap<String, ArrayList<String>>> relType = new HashMap<String, HashMap<String, ArrayList<String>>>();
		HashMap<String, HashMap<String, HashMap<String, String>>> sourceInfo = new HashMap<String, HashMap<String, HashMap<String, String>>>();
		
		BuildTree b = new BuildTree();
		
		System.out.println(yagoTypes.size());
		String line;
		String pattern1 = "^\\((.*)\\)$";
		String pattern2 = "(\\[[0-9]*[a-z]*\\])";
		Pattern r = Pattern.compile(pattern1);
		Pattern r1 = Pattern.compile(pattern2);
		while((line=br1.readLine())!=null)
		{
			//System.out.println(line);
			StringTokenizer tokenizer = new StringTokenizer(line, "\t");
			if(!tokenizer.hasMoreTokens()) continue;
			ArrayList<String> tokens = new ArrayList<String>();
			
			while(tokenizer.hasMoreTokens())
			{
				tokens.add(tokenizer.nextToken());
			}
			if(tokens.size()>4 || tokens.size()<4) continue;
			String a1 = tokens.get(0).trim().replace("<code>","").replace("</code>", "").replace("[htt","").replace("[http","").replace("<br","").replace("List of ","").replace("<br />", "").replace("<small>", "").replace("</small>", "").replace("{{note|a}}", "").replace("</dt", "").replace(">", "").replace("</dd", "");
			
			String a3 = tokens.get(1).trim();
			String a2 = tokens.get(2).trim().replace("<code>","").replace("</code>", "").replace("<br","").replace("[htt","").replace("[http","").replace("List of ","").replace("<br />", "").replace("<small>", "").replace("</small>", "").replace("{{note|a}}", "").replace("</dt", "").replace(">", "").replace("</dd", "");
			String a4 = tokens.get(3);
			if(a1.contains("|"))
			{
				a1 = a1.substring(0,a1.indexOf('|'));
			}
			if(a2.contains("|"))
			{
				a2 = a2.substring(0,a2.indexOf('|'));
			}
			if(a2.startsWith("list of")) a2 = a2.replace("list of","");
			if(a1.startsWith("list of")) a1 = a1.replace("list of","");
			//String a4 = tokens.get(3);
			if(a1.contains("<span style=") || a2.contains("<span style=")||a1.equals("") || a2.equals("") || a3.equals("") || a1.contains("{{") || a1.contains("}}") || a2.contains("{{") || a2.contains("}}") || a3.contains("{{") || a3.contains("}}") ) continue;
			Matcher m1 = r.matcher(a1);
		/*	if(m1.find()) 
			{
				bw3.write(a1+"\n");
				a1 = a1.replace("(", "").replace(")", "");
				if(a1.charAt(a1.length()-1)=='_')
					a1 = a1.substring(0, a1.length()-1);
				
				//a1 = b.stem(b.removeDisambiguation(a1));
			}*/
			a1 = a1.trim();
			
			if(a1.charAt(0)=='(' && a1.charAt(a1.length()-1)==')')
			{
				bw3.write(a1+"\n");
				//a1 = a1.replace("(", "").replace(")", "");
				a1 = changeCharInPosition(0,(char) 0,a1);
				a1 = changeCharInPosition(a1.length()-1,(char) 0, a1).trim();
				
				/*if(a1.charAt(a1.length()-1)=='_')
					a1 = a1.substring(0, a1.length()-1);*/
				
				//a1 = b.stem(b.removeDisambiguation(a1));
			}
			Matcher m2 = r1.matcher(a1);
			if(m2.find())
			{
				bw3.write("reference: "+a1+"\t");
				a1 = a1.replace(m2.group(1), "");
				bw3.write(a1+"\n");
			}
		/*	bw3.write(a1+"\n");
			bw3.write(b.removeDisambiguation(a1)+"\n");
			bw3.write(b.stem(b.removeDisambiguation(a1))+"\n");
			bw3.write(b.stem(b.removeDisambiguation(a1)).trim().toLowerCase().replace(" ", "_")+"\n");*/
		/*	Matcher m = r.matcher(a2);
			if(m.find()) 
				{
				bw3.write(a2+"\n");
				a2 = a2.replace("(", "").replace(")", "").trim();
				if(a2.charAt(a2.length()-1)=='_')
					a2 = a2.substring(0, a2.length()-1);
				//a2 = b.stem(b.removeDisambiguation(a2));
				}*/
			if(a2.charAt(0)=='(' && a2.charAt(a2.length()-1)==')')
			{
				bw3.write(a2+"\n");
				//a1 = a1.replace("(", "").replace(")", "");
				a2 = changeCharInPosition(0,(char) 0,a2);
				a2 = changeCharInPosition(a2.length()-1,(char) 0, a2).trim();
				
				/*if(a1.charAt(a1.length()-1)=='_')
					a1 = a1.substring(0, a1.length()-1);*/
				
				//a1 = b.stem(b.removeDisambiguation(a1));
			}
			
			Matcher m3 = r1.matcher(a2);
			if(m3.find())
			{
				bw3.write("reference: "+a2+"\n");
				a2 = a2.replace(m3.group(1), "");
				bw3.write(a2+"\n");
			}
		/*	bw3.write(a2+"\n");
			bw3.write(b.removeDisambiguation(a2)+"\n");
			bw3.write(b.stem(b.removeDisambiguation(a2))+"\n");
			bw3.write(b.stem(b.removeDisambiguation(a2)).trim().toLowerCase().replace(" ", "_")+"\n");*/
			String newName1 = b.stem(b.removeDisambiguation(b.removeDisambiguation(a1).trim())).trim().toLowerCase().replace(" ", "_");
			String newName2 = b.stem(b.removeDisambiguation(b.removeDisambiguation(a2).trim())).trim().toLowerCase().replace(" ", "_");
			if(newName1.equals(newName2) || newName1.equals("") || newName2.equals("")) continue;
			if(newName1.length()==1 || newName2.length()==1) continue;
			if(newName1.equals("try")) newName1 = "trie";
			if(newName2.equals("try")) newName2 = "trie";
			String newa4="";
			if(a4.contains(":Template"))
			{
				newa4 = a4.replace(":Template", "").replace("/wiki/", "").toLowerCase();
			}
			
			if(newName1.endsWith("graphic")) newName1=newName1.replace("graphic","graphics");
			if(newName2.endsWith("graphic")) newName2=newName2.replace("graphic","graphics");
			
			if(newName1.endsWith("mathematic")) newName1=newName1.replace("mathematic", "mathematics");
			if(newName2.endsWith("mathematic")) newName2=newName2.replace("mathematic", "mathematics");
			
			if(newName1.endsWith("databas")) 
			{
				bw3.write(newName1+" 1\t");
				newName1=newName1.replace("databas", "database");
				bw3.write("found online database: "+newName1+"\t"+line+"\n");
			}
			if(newName2.endsWith("databas"))
			{
				bw3.write(newName2+" 2\t");
				newName2=newName2.replace("databas", "database");
				bw3.write("found online database: "+newName2+"\t"+line+"\n");
			}
			if(newName1.startsWith("list_of")) newName1 = newName1.replace("list_of_","");
			if(newName2.startsWith("list_of")) newName2 = newName2.replace("list_of_","");
			if(yagoTypes.get(newName1)!=null)
			{
				if(yagoTypes.get(newName1).contains("people") || yagoTypes.get(newName1).contains("organization") || yagoTypes.get(newName1).contains("building") || yagoTypes.get(newName1).contains("yagoGeoEntity") || yagoTypes.get(newName1).contains("music") || newName1.startsWith("list_of_")) continue;
			}
			if(yagoTypes.get(newName2)!=null)
			{
				if(yagoTypes.get(newName2).contains("people") || yagoTypes.get(newName2).contains("organization") || yagoTypes.get(newName2).contains("building") || yagoTypes.get(newName2).contains("yagoGeoEntity") || yagoTypes.get(newName2).contains("music") || newName2.startsWith("list_of_")) continue;
			}
			if(yagoTypes.get(newa4)!=null)
			{
				if(yagoTypes.get(newa4).contains("people") || yagoTypes.get(newa4).contains("organization") || yagoTypes.get(newa4).contains("building") || yagoTypes.get(newa4).contains("yagoGeoEntity") || yagoTypes.get(newa4).contains("music") || newa4.startsWith("list_of_")) continue;
			}
			if(newName1.equals("other") || newName1.startsWith("other") || newName1.equals("example") || newName1.startsWith("example") || newName2.equals("other") || newName2.startsWith("other")|| newName2.startsWith("example") || newName2.equals("example")) continue;
			if(newName1.startsWith("file:") || newName1.startsWith("image:") || newName2.startsWith("file:") || newName2.startsWith("image:")) continue;
			
			if(a4.contains(": List Hierarchy"))
			{
				String a6 = a4.replace(": List Hierarchy", "").replace("/wiki/", "").toLowerCase();
				if(a3.equals("typeOf"))
				{
					int ind = a6.lastIndexOf("list_of");
					String a5 = a6.substring(ind).replace("list_of_", "");
					if(a5.lastIndexOf("#")!=-1)
					{
						a5 = b.stem(a5.substring(0,a5.lastIndexOf("#") ));
					}
					else
					{
						a5 = b.stem(a5);
					}
					if(!newName1.contains(a5)) newName1 = newName1 + "_"+a5;
					if(!newName2.contains(a5)) newName2 = newName2 + "_"+a5;
				}
			}
			if(a4.equals("wikipedia_list"))
			{
				//System.out.println("wl");
				if(isYear(newName1) || isYear(newName2)) continue;
			}
			String[] nn1 = newName1.split("_");
			String[] nn2 = newName2.split("_");
			if(nn1.length>8 || nn2.length>8) continue;
			Edge e = new Edge();
			e.setName(newName2);
			e.setRelationName(a3);
			ArrayList<Edge> list = types.get(newName1);
			if(list==null)
			{
				list = new ArrayList<Edge>();
			}
			
			boolean flag =  false;
			for(Edge e1:list)
			{
				if(e1.getName().equals(e.getName()) && e1.getRelationaName().equals(e.getRelationaName())) 
					{
					flag = true;
					break;
					}
			}
			
			if(!flag)	list.add(e);
			types.put(newName1, list);
			if(sourceInfo.get(newName1)==null)
			{
				HashMap<String, HashMap<String, String>> h1 = new HashMap<String, HashMap<String, String>>();
				HashMap<String, String> h2 = new HashMap<String, String>();
				h2.put(newName2, a4);
				h1.put(a3, h2);
				sourceInfo.put(newName1, h1);
			}
			else
			{
				HashMap<String, HashMap<String, String>> h1 = sourceInfo.get(newName1);
				if(h1.get(a3)==null)
				{
					HashMap<String, String> h2 = new HashMap<String, String>();
					h2.put(newName2, a4);
					h1.put(a3, h2);
				}
				else
				{
					HashMap<String, String> h2 = h1.get(a3);
					h2.put(newName2, a4);
					h1.put(a3, h2);
				}
				sourceInfo.put(newName1, h1);
			}
			if(relType.get(newName1)!=null)
			{
				HashMap<String, ArrayList<String>> hm1 = relType.get(newName1);
				if(hm1.get(newName2)!=null)
				{
					ArrayList<String> relList = (ArrayList<String>) hm1.get(newName2);
					if(!relList.contains(a3))
						relList.add(a3);
					hm1.put(newName2, relList);
				}
				else
				{
					ArrayList<String> relList = new ArrayList<String>();
					relList.add(a3);
					hm1.put(newName2, relList);
				}
				//hm1.put(d, b);
				relType.put(newName1, hm1);
			}
			else if(relType.get(newName1)==null)
			{
				HashMap<String, ArrayList<String>> hm1 = new HashMap<String, ArrayList<String>>();
				ArrayList<String> relList  = new ArrayList<String>();
				relList.add(a3);
				hm1.put(newName2, relList);
				relType.put(newName1, hm1);
			}
			if(relType.get(newName2)==null) relType.put(newName2, new HashMap<String, ArrayList<String>>());
			
			
		}
		/*while((line=br2.readLine())!=null)
		{
			StringTokenizer tokenizer = new StringTokenizer(line, "\t");
			if(!tokenizer.hasMoreTokens()) continue;
			ArrayList<String> tokens = new ArrayList<String>();
			while(tokenizer.hasMoreTokens())
			{
				tokens.add(tokenizer.nextToken());
			}
			if(tokens.size()>4 || tokens.size()<4) continue;
			
			String a1 = tokens.get(0);
			String a3 = tokens.get(1);
			String a2 = tokens.get(2);
			String a4 = tokens.get(3);
			if(a1.equals("") || a2.equals("") || a3.equals("")) continue;
			Matcher m1 = r.matcher(a1);
			if(m1.find()) 
				{
				//System.out.println(newName1);
				a1 = a1.replace("(", "").replace(")", "");
				if(a1.charAt(a1.length()-1)=='_')
					a1 = a1.substring(0, a1.length()-1);
				
				//a1 = b.stem(b.removeDisambiguation(a1));
				}
			
			Matcher m = r.matcher(a2);
			if(m.find()) 
				{
				//System.out.println(newName2);
				a2 = a2.replace("(", "").replace(")", "").trim();
				if(a2.charAt(a2.length()-1)=='_')
					a2 = a2.substring(0, a2.length()-1);
				//a2 = b.stem(b.removeDisambiguation(a2));
				}
			String newName1 = b.stem(b.removeDisambiguation(a1)).trim().toLowerCase().replace(" ", "_");
			String newName2 = b.stem(b.removeDisambiguation(a2)).trim().toLowerCase().replace(" ", "_");
			if(newName1.equals(newName2)) continue;
			if(newName1.equals("try")) newName1 = "trie";
			if(newName2.equals("try")) newName2 = "trie";
			if(newName1.length()==1 || newName2.length()==1) continue;
			
			
			Edge e = new Edge();
			e.setName(newName2);
			e.setRelationName(a3);
			ArrayList<Edge> list = types.get(newName1);
			if(list==null)
			{
				list = new ArrayList<Edge>();
			}
			
			boolean flag =  false;
			for(Edge e1:list)
			{
				if(e1.getName().equals(e.getName()) && e1.getRelationaName().equals(e.getRelationaName())) 
				{
					flag = true;
					break;
				}
			}
			
			if(!flag)	list.add(e);
			if(types.get(e.getName())==null)
			{
				types.put(e.getName(), new ArrayList<Edge>());
			}
			types.put(newName1, list);
			if(relType.get(newName1)!=null)
			{
				HashMap<String, ArrayList<String>> hm1 = relType.get(newName1);
				if(hm1.get(newName2)!=null)
				{
					ArrayList<String> relList = (ArrayList<String>) hm1.get(newName2);
					if(!relList.contains(a3))
						relList.add(a3);
					hm1.put(newName2, relList);
				}
				else
				{
					ArrayList<String> relList = new ArrayList<String>();
					relList.add(a3);
					hm1.put(newName2, relList);
				}
				//hm1.put(d, b);
				relType.put(newName1, hm1);
			}
			else if(relType.get(newName1)==null)
			{
				HashMap<String, ArrayList<String>> hm1 = new HashMap<String, ArrayList<String>>();
				ArrayList<String> relList  = new ArrayList<String>();
				relList.add(a3);
				hm1.put(newName2, relList);
				relType.put(newName1, hm1);
			}
			if(relType.get(newName2)==null) relType.put(newName2, new HashMap<String, ArrayList<String>>());
				
		}*/
		AdjList a = new AdjList(types);
		a.setRelTypes(relType);
		a.writeToFileWithSource(bw1, sourceInfo);
		a.separateTransitiveTriplesWithSource(bw2, sourceInfo);
		System.out.println("written to file!");
		System.out.println("number of concepts: "+a.adjList.size());
		ArrayList<String> keys = new ArrayList<String>();
		for(String key:a.adjList.keySet())
		{
			keys.add(key);
		}
		Collections.sort(keys, new Comparator<String>() {

	        public int compare(String o1, String o2) 
	        {
	        	String[] a1 = o1.split("_");
	        	String[] a2 = o2.split("_");
	        	Integer i1 = a1.length;
	        	Integer i2 = a2.length;
	            return i2.compareTo(i1);
	        }
	    });
		for(String key:keys)
		{
			bw4.write(key+"\n");
		}
		
	/*	String outLinksFile = hm.get("outlinks-file");
		HashMap<String, Integer> outlinksnum = new HashMap<String, Integer>();
		BufferedReader brr = new BufferedReader(new FileReader(outLinksFile));
		String line1;
		while((line1=brr.readLine())!=null)
		{
			StringTokenizer tok = new StringTokenizer(line1,"\t");
			String name = tok.nextToken();
			String numbername = tok.nextToken();
			if(NumberUtils.isNumber(numbername)) 
			{
				outlinksnum.put(name.trim().replace(" ", "_").toLowerCase(), Integer.parseInt(numbername));
			}
		}
		brr.close();
		
		
	//	HashMap<String, ArrayList<String>> h2 = a.returnStringHashMap();
		
		//HashMap<String, ArrayList<String>> adjList = b1.handleRedirectionsAndCategories(h2, hm.get("redirections"), hm.get("yago-simple-types"), hm.get("yago-transitive-types"), outlinksnum);
	   //  System.out.println(adjList.size()); 
	/*	 Iterator it3 = types.entrySet().iterator();
	    while(it3.hasNext())
	    {
	    	Map.Entry pair = (Map.Entry)it3.next();
	    	String name1 = (String) pair.getKey();
	    	ArrayList<String> list1=(ArrayList<String>) pair.getValue();
	    	
			
	    	
	    }*/
	    br1.close();
	    //br2.close();
	    bw1.close();
	    bw2.close();
	    bw3.close();
	    bw4.close();
	}
	
	

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception
	{
		// TODO Auto-generated method stub
		GetPropertyValues properties = new GetPropertyValues();
		HashMap<String, String> hm = properties.getPropValues();
		BufferedReader br1 = new BufferedReader(new FileReader(hm.get("toc-filtered")));
		BufferedReader br2 = new BufferedReader(new FileReader(hm.get("triples1")));
		BuildTree b = new BuildTree();
		HashMap<String, String> yagoTypes = b.readYAGOTypes(hm.get("yago-simple-types"), hm.get("yago-transitive-types"));
		cleanTriples(br1,hm,"/mnt/dell/prajna/neo4j/input/knowledgeBase/entireKB/tocFilteredTransitive","/mnt/dell/prajna/neo4j/input/knowledgeBase/entireKB/tocFilteredNoTransitiveTriples","/mnt/dell/prajna/neo4j/input/knowledgeBase/entireKB/tocFilteredOut","/mnt/dell/prajna/neo4j/input/knowledgeBase/entireKB/tocFilteredEntities", yagoTypes);
		cleanTriples(br2, hm,"/mnt/dell/prajna/neo4j/input/knowledgeBase/entireKB/KBWithSingularNamesAndDuplicatesRemovedFinal1","/mnt/dell/prajna/neo4j/input/knowledgeBase/entireKB/KBWithNoTransitiveTriplesFinal1","/mnt/dell/prajna/neo4j/input/knowledgeBase/entireKB/out.txt","/mnt/dell/prajna/neo4j/input/knowledgeBase/entireKB/entities.txt", yagoTypes);
		BufferedReader br3 = new BufferedReader(new FileReader("/mnt/dell/prajna/neo4j/input/knowledgeBase/entireKB/tocFilteredNoTransitiveTriples"));
		HashMap<String, ArrayList<Edge>> types = new HashMap<String, ArrayList<Edge>>();
		HashMap<String, HashMap<String, ArrayList<String>>> relType = new HashMap<String, HashMap<String, ArrayList<String>>>();
		HashMap<String, HashMap<String, HashMap<String, String>>> sourceInfo = new HashMap<String, HashMap<String, HashMap<String, String>>>();
		
		String line;
		while((line=br3.readLine())!=null)
		{
			StringTokenizer tokenizer = new StringTokenizer(line,"\t");
			ArrayList<String> tokens = new ArrayList<String>();
			
			while(tokenizer.hasMoreTokens())
			{
				tokens.add(tokenizer.nextToken());
			}
			String newName1 = tokens.get(0);
			String newName2 = tokens.get(2);
			String a3 = tokens.get(1);
			String a4 = tokens.get(3);
			Edge e = new Edge();
			e.setName(tokens.get(2));
			e.setRelationName(tokens.get(1));
			ArrayList<Edge> list = types.get(newName1);
			if(list==null)
			{
				list = new ArrayList<Edge>();
			}
			
			boolean flag =  false;
			for(Edge e1:list)
			{
				if(e1.getName().equals(e.getName()) && e1.getRelationaName().equals(e.getRelationaName())) 
					{
					flag = true;
					break;
					}
			}
			
			if(!flag)	list.add(e);
			types.put(newName1, list);
			if(sourceInfo.get(newName1)==null)
			{
				HashMap<String, HashMap<String, String>> h1 = new HashMap<String, HashMap<String, String>>();
				HashMap<String, String> h2 = new HashMap<String, String>();
				h2.put(newName2, a4);
				h1.put(a3, h2);
				sourceInfo.put(newName1, h1);
			}
			else
			{
				HashMap<String, HashMap<String, String>> h1 = sourceInfo.get(newName1);
				if(h1.get(a3)==null)
				{
					HashMap<String, String> h2 = new HashMap<String, String>();
					h2.put(newName2, a4);
					h1.put(a3, h2);
				}
				else
				{
					HashMap<String, String> h2 = h1.get(a3);
					h2.put(newName2, a4);
					h1.put(a3, h2);
				}
				sourceInfo.put(newName1, h1);
			}
			if(relType.get(newName1)!=null)
			{
				HashMap<String, ArrayList<String>> hm1 = relType.get(newName1);
				if(hm1.get(newName2)!=null)
				{
					ArrayList<String> relList = (ArrayList<String>) hm1.get(newName2);
					if(!relList.contains(a3))
						relList.add(a3);
					hm1.put(newName2, relList);
				}
				else
				{
					ArrayList<String> relList = new ArrayList<String>();
					relList.add(a3);
					hm1.put(newName2, relList);
				}
				//hm1.put(d, b);
				relType.put(newName1, hm1);
			}
			else if(relType.get(newName1)==null)
			{
				HashMap<String, ArrayList<String>> hm1 = new HashMap<String, ArrayList<String>>();
				ArrayList<String> relList  = new ArrayList<String>();
				relList.add(a3);
				hm1.put(newName2, relList);
				relType.put(newName1, hm1);
			}
			if(relType.get(newName2)==null) relType.put(newName2, new HashMap<String, ArrayList<String>>());
			
		
		}
		BufferedReader br4 = new BufferedReader(new FileReader("/mnt/dell/prajna/neo4j/input/knowledgeBase/entireKB/KBWithNoTransitiveTriplesFinal1"));
		BufferedWriter bw4 = new BufferedWriter(new FileWriter("/mnt/dell/prajna/neo4j/input/knowledgeBase/entireKB/KBWithNoTransitiveTriplesFinal2"));
		String line1;
		while((line1=br4.readLine())!=null)
		{
			StringTokenizer tokenizer1 = new StringTokenizer(line1,"\t");
			ArrayList<String> a = new ArrayList<String>();
			while(tokenizer1.hasMoreTokens())
			{
				a.add(tokenizer1.nextToken());
			}
			if(!a.get(3).contains(":TOC"))
			{
				bw4.write(a.get(0)+"\t"+a.get(1)+"\t"+a.get(2)+"\t"+a.get(3)+"\n");
			}
			else
			{
				if(relType.get(a.get(0))!=null)
				{
					ArrayList<String> relationList = relType.get(a.get(0)).get(a.get(2));
					if(relationList!=null)
					{
						if(a.get(1).equals("typeOf"))
						{
							String newRel="typeOf";
							for(String rel:relationList)
							{
								if(rel.contains("type"))
								{
									newRel=rel;
								}
							}
							bw4.write(a.get(0)+"\t"+newRel+"\t"+a.get(2)+"\t"+a.get(3)+"\n");
							
						}
						else
						{
							bw4.write(a.get(0)+"\t"+a.get(1)+"\t"+a.get(2)+"\t"+a.get(3)+"\n");
						}
					}
					else
					{
						bw4.write(a.get(0)+"\t"+a.get(1)+"\t"+a.get(2)+"\t"+a.get(3)+"\n");
					}
				}
				else
				{
					bw4.write(a.get(0)+"\t"+a.get(1)+"\t"+a.get(2)+"\t"+a.get(3)+"\n");
				}
				
			}
			
		}
		br1.close();
		br2.close();
		br3.close();
		br4.close();
		bw4.close();
		
	}
	public static boolean isYear(String str)
	{
		if(str.length()!=4) return false;
		for(char c:str.toCharArray())
		{
			if(!Character.isDigit(c)) return false;
		}
		
		int year = Integer.parseInt(str);
		if(year>2020) return false;
		
		return true;
	}
	public static String changeCharInPosition(int position, char ch, String str)
	{
	    char[] charArray = str.toCharArray();
	    charArray[position] = ch;
	    return new String(charArray);
	}

}
