
fileName = 'latestkb.csv'
import csv
import urllib2 
from bs4 import BeautifulSoup
import string
import unicodedata

#divide triples as new,retained,discarded
newFile = 'list_hierarchy_new_collection.txt'
retainFile = 'list_hierarchy_retained.txt'
rubbish = 'lh_discarded.txt'
targetnew = open(newFile, 'w')
targetret = open(retainFile,'w')
throw = open(rubbish,'w')

#read all the lines in kb
csvfile = open(fileName)
readerFile = csv.reader(csvfile)
data = list(readerFile)
main_count = len(data)
print main_count

#Some counts to know stats
del_count = 0 
retained = 0 
base_url = 'https://en.wikipedia.org'

#filter these headings out
unwanted = ['Contents','See also','References','External links','Footnotes','Further reading']

#lists for some checks
source_list = []
fresh_triples = []

#Utility Functions
def cleanSourceString(source):
	source = source.replace('\n','')
	source = source.replace('\\','')
	source = source.replace(' n','')
	return source
	
def getContent(main_url):
	print '\n==================',main_url,'====================\n'
	try:
		page = urllib2.urlopen(main_url)
	except:
		print 'not found'	
	soup = BeautifulSoup(page,"lxml")
	condiv = soup.findAll('div', id="mw-content-text")
	return condiv
	
def validateTypeOf(condiv, triple):
	print 'validating typeOf'
	allh2 = condiv[0].findAll('h2')	
	e1 = triple[0]
	el1 = ' '.join(e1.split('_'))
	for heading in allh2:
		curr = heading.next_sibling
		while (curr and curr.name != 'ul') and (curr and curr.name != 'h2'):
			if curr.name == 'ul' or curr.name == 'dl' or curr.name == 'h3':
				break
			curr = curr.next_sibling
			if curr and curr.name == 'ul' and triple[1].find('typeOf') != -1:							
				ulist1 = curr.contents							
				ulist1_ref =filter(lambda a: a != u'\n', ulist1)
				#print ulist1_ref
				flag = False
				for ele in ulist1_ref:
					hyperlink = ele.find('a')		
					if hyperlink:
						k = hyperlink.text
						htext = unicodedata.normalize('NFKD', k).encode('ascii','ignore')
						print htext.lower(),' e1 ',el1
																	
					if hyperlink != None and hyperlink.text.lower() == el1:
						k = hyperlink.text
						htext = unicodedata.normalize('NFKD', k).encode('ascii','ignore')
						print hyperlink.text , ' Keeping Link Entity'									
						#target.write(htext+'\ttypeOf\t'+l+'\t'+source)
						#target.write('\n')
						#retained += 1
	
def correctEntity(heading):
	hyperlink = heading.find('a')
	plain = heading.text
	normal_entity = unicodedata.normalize('NFKD', plain).encode('ascii','ignore')		
	normal_entity = normal_entity.replace('[edit]','')
	if hyperlink:
		if hyperlink.text == 'edit':
			nsib = heading.find_next_sibling('div')
			if nsib and	nsib.text.find('Main article') != -1:
				entity1 = nsib.find('a').text
				return entity1
			else:				
				return normal_entity
	return normal_entity		
			
			
def addFreshHeadingRelations(condiv,triple):
	global fresh_triples
	allh2 = condiv[0].findAll('h2')	
	for heading in allh2:
		entity = correctEntity(heading)
		if entity not in unwanted:
			entity = '_'.join(entity.split(' ')).lower()
			nt = entity+'\tisTerminology(relatedTo)\t'+triple[2]+'\t'+source	
			fresh_triples.append(nt)
			print 'fresh : \t',nt	
							
		
def filterTriple(triple,url):
	print 'filtering:\t',triple
	triple = '\t'.join(triple)
	if triple not in fresh_triples:
		print 'discarded:\t',triple
	
#process each triple if the source is list hierarchy
for line in data:
	triple = line[0:4]
	#print triple	
	if len(triple) == 4:
		e1 = triple[0]
		e2 = triple[2]
		reln = triple[1]
		el1 = ' '.join(e1.split('_'))
		el2 = ' '.join(e2.split('_'))
		source = triple[3]
		source = cleanSourceString(source)		
		#print source		
		"""process this triple only if its list hierarchy source extraction"""
		if source.lower().find('list hierarchy') != -1:
			addr = source[0:source.find(':')]
			main_url = base_url + addr
			condiv = getContent(main_url)
			if len(condiv) > 0:
				if main_url not in source_list:
					addFreshHeadingRelations(condiv,triple)
					source_list.append(main_url)
					triple_tsv = '\t'.join(triple)
					if triple_tsv in fresh_triples:
						print 'retained:\t',triple_tsv
					else:
						print 'improper: \t',triple
				else:
					if reln.find('type') != -1:
						validateTypeOf(triple,condiv)
					else:
						filterTriple(triple,main_url)
		"""write code from here"""
		"""
				allh2 = condiv[0].findAll('h2')
				fresh_ens = []
				for heading in allh2:
					#for keyword in headingList:
					k = heading.text
					l = unicodedata.normalize('NFKD', k).encode('ascii','ignore')
					#	if l.lower().find(keyword) != -1:
					#		knownHeadings.append(l)
					#		h2name = l
					#print heading.text
					l = l.replace('[edit]','')
					hyperlink = heading.find('a')					
					nt = l+'\tisTerminology(relatedTo)\t'+triple[2]+'\t'+source						
					if hyperlink:
						#print l
						if hyperlink.text == 'edit':
							nsib = heading.find_next_sibling('div')
							if nsib and	nsib.text.find('Main article') != -1 and nt not in new_trips and not seen:
								entity1 = nsib.find('a').text
								if entity1.find('List') == -1:
									nt = entity1+'\tisTerminology(relatedTo)\t'+triple[2]+'\t'+source
									print 'corrct name\n',nt
									target.write(l+'\tisTerminology(relatedTo)\t'+triple[2]+'\t'+source)
									target.write('\n')
									new_trips.append(nt)
								
										
					if nt not in new_trips and l not in unwanted and l.lower().find('other') == -1 and hyperlink and hyperlink.text != 'edit':
						
						print l+'\tisTerminology(relatedTo)\t'+triple[2]+'\t'+source
						target.write(l+'\tisTerminology(relatedTo)\t'+triple[2]+'\t'+source)
						target.write('\n')
						new_trips.append(nt)
						curr = heading.next_sibling
						while (curr and curr.name != 'ul') and (curr and curr.name != 'h2'):
							if curr.name == 'ul' or curr.name == 'dl' or curr.name == 'h3':
								break
							curr = curr.next_sibling	
							if curr and curr.name == 'h3':
								hyperlink = curr.find('a')					
								nt = str(hyperlink.text)+'\tisTerminology(relatedTo)\t'+l+'_'+triple[2]+'\t'+source			
								if hyperlink != None and hyperlink.text.lower() != 'edit' and nt not in new_trips and not seen:
									k = heading.text
									htext = unicodedata.normalize('NFKD', k).encode('ascii','ignore')
									print htext , ' h3 to h2'									
									target.write(htext+'\tisTerminology(relatedTo)\t'+l+'_'+triple[2]+'\t'+source)
									target.write('\n')
									fresh_ens.append(htext)
									new_trips.append(nt)
								
							if curr and curr.name == 'ul' and triple[1].find('typeOf') != -1:							
								ulist1 = curr.contents							
								ulist1_ref =filter(lambda a: a != u'\n', ulist1)
								#print ulist1_ref
								flag = False
								for ele in ulist1_ref:
									hyperlink = ele.find('a')		
									if hyperlink:
										k = hyperlink.text
										htext = unicodedata.normalize('NFKD', k).encode('ascii','ignore')
										print htext.lower(),' e1 ',el1
																					
									if hyperlink != None and hyperlink.text.lower() == el1:
										k = hyperlink.text
										htext = unicodedata.normalize('NFKD', k).encode('ascii','ignore')
										print hyperlink.text , ' Keeping Link Entity'									
										target.write(htext+'\ttypeOf\t'+l+'\t'+source)
										target.write('\n')
										retained += 1
									elif hyperlink and hyperlink.text.find('http') == -1:
										k = heading.text
										htext = unicodedata.normalize('NFKD', k).encode('ascii','ignore')
										htext = htext.replace('[edit]','')
										#print l, '-------------l',
										nt = htext+'\tisTerminology(relatedTo)\t'+l+'\t'+source
										if htext not in fresh_ens and htext != l and not seen:
											print ' list ele ', nt,'   ========== new relation'								
											target.write(htext+'\t'+'\ttypeOf\t'+l+'\t'+source)
											target.write('\n')
											new_trips.append(nt)
							else:
								t = '\t'.join(triple[0:4])
								if t not in retained_t:
									print 'retained\n',t
									target.write(t)
									target.write('\n')
									retained_t.append(t)
				
	"""					
							
							
			
	
		

	
