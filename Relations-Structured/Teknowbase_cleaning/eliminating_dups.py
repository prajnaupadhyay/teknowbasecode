fileName = 'tkb_seclist_cleaned.csv'
newFile = 'tkb_no_dups.txt'
import csv

csvfile = open(fileName)
readerFile = csv.reader(csvfile)
data = list(readerFile)
print len(data)
print data[0]
clean_data = []
for line in data:
	clean_data.append(line[0])
	
clean_data = list(set(clean_data))
print len(clean_data)

target = open(newFile,'w')

entity_list = []
for line in clean_data:
		triple = line.split('\t')
		if len(triple) >= 3:		
			e1 = triple[0]
			e2 = triple[2]
			entity_list.append(e1)
			entity_list.append(e2)
			target.write(line)
			target.write('\n')
		
entity_list = list(set(entity_list))
target.close()
	
efile = 'entities_cleaned.txt'
target = open(efile,'w')
for en in entity_list:
	target.write(en)
	target.write('\n')
