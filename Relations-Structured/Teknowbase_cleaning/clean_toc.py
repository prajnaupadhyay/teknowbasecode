
fileName = 'latestkb.csv'
import csv
import urllib2 
from bs4 import BeautifulSoup
import string
import unicodedata
import helper

#Files for new storage
newFile = 'tkb_cleaned_toc.txt'
rubbish = 'toc_discarded.txt'
throw = open(rubbish,'w')
target = open(newFile, 'w')
csvfile = open(fileName)
readerFile = csv.reader(csvfile)

data = list(readerFile)
main_count = len(data)
print main_count
count = 0 
retained = 0 
base_url = 'https://en.wikipedia.org'

unwanted = ['Contents','See also','References','External links','Footnotes','Further reading']
source_list = []

def examineTocEntity(heading):
	hyperlink = heading.find('a')
	plain = heading.text
	normal_entity = unicodedata.normalize('NFKD', plain).encode('ascii','ignore')		
	normal_entity = normal_entity.replace('[edit]','')
	normal_entity = normal_entity.replace('edit','')
	if hyperlink:
		if hyperlink.text == 'edit':
			nsib = heading.find_next_sibling('div')
			if nsib and	(nsib.text.find('Main article') != -1 or nsib.text.find('Further') != -1 ):
				k = nsib.find('a').text
				entity1 = unicodedata.normalize('NFKD', k).encode('ascii','ignore')
				return True,entity1
			else:				
				return False,normal_entity
		elif hyperlink.text.find('http') == -1 and hyperlink.text.lower().find('list') == -1:
			return True,normal_entity
	return False,normal_entity
			
def checkHlist(allh,el1,triple):
	global target
	global throw
	for heading in allh:
		k = heading.text
		htext = unicodedata.normalize('NFKD', k).encode('ascii','ignore')
		htext = htext.replace('[edit]','')
		htext = htext.replace('edit','')
		#print htext,' ',el1
		if htext.lower().find(el1) != -1:
			isValid,entity1 = examineTocEntity(heading)
			if isValid and entity1.lower().find('list of') == -1:
				print 'retained: ',entity1+'\t'+'\t'.join(triple[1:3])+'\t'+source
				entity1 = '_'.join(entity1.split(' ')).lower()
				target.write(entity1+'\t'+'\t'.join(triple[1:3])+'\t'+source)
				target.write('\n')
			else:
				print 'discarded: ',triple
				throw.write('\t'.join(triple))
				throw.write('\n')
	
							
for line in data:
	#print line
	triple = line[0:4]
	source = helper.cleanSourceString(triple[3])	
	e1 = triple[0]
	e2 = triple[2]
	reln = triple[1]
	el1 = ' '.join(e1.split('_'))
	el2 = ' '.join(e2.split('_'))
	source = triple[3]
	source = helper.cleanSourceString(source)				
	#print '\n******************\n',source		
	"""process this triple only if its list hierarchy source extraction"""
	if source.lower().find(':toc') != -1:
		addr = source[0:source.find(':')]
		main_url = base_url + addr
		condiv = helper.getContent(main_url)
		if len(condiv) > 0:
			allh2 = condiv[0].findAll('h2')
			allh3 = condiv[0].findAll('h3')
			allh4 = condiv[0].findAll('h4')
			print '\ncheck all h2\n'
			checkHlist(allh2,el1,triple)
			print '\ncheck all h3\n'
			checkHlist(allh3,el1,triple)
			print '\ncheck all h4\n'
			checkHlist(allh4,el1,triple)
	
	
		

	

