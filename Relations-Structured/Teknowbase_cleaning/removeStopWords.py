# file that reads the KB and returns the number of triples that contain stop words in their entities

fileName = 'tkbLatest.csv'
import csv

removeFile = open('remove_these.txt')
stopWords = removeFile.readlines()
for word in stopWords:
	word.strip('\n')
	word = word.replace('\n','')
	#print word


csvfile = open(fileName)
readerFile = csv.reader(csvfile)
data = list(readerFile)
main_count = len(data)
print main_count
count = 0
for line in data:
	triple = line[0].split('\t')
	if len(triple) == 4:
		e1 = triple[0]
		e2 = triple[2]
		el1 = ' '.join(e1.split('_'))
		el2 = ' '.join(e2.split('_'))
		flag = False
		for sw in stopWords:
			if el1.find(sw) == -1 and el2.find(sw) == -1:
				flag = True
		if flag:
			count += 1
		#print el1,'\t\t',el2

print count
	

