Heuristic: heuristicSet1

Code: /mnt/dell/teknowbase/Code/Relations-Structured/Teknowbase_cleaning/CleanTriples.java
Author:	Prajna
Purpose: 1) stemming of entities, 
		 2) removing disambiguation, 
		 3) removing HTML tags, 
		 4) removing reference symbols, 
		 5) entities that start with "file:", "image: ", 
		 6) entities that are names of organizations, buildings, geoentity, music etc, 
		 7) removed triples that contained entities having more than 8 words, 
		 8) fixed triples of the form: "Abstract algebra (algorithms#Error detection and correction), typeOf, Computational mathematics" to "abstract_algebra_algorithm, typeOf, computational_mathematics_algorithm"
Input: List of triples	
Output: Refined list of triples

---------------------------------------------------


Heuristic: section_lists_filtered

Code: /mnt/dell/teknowbase/Code/Relations-Structured/Teknowbase_cleaning/clean_section_list_non_links.py
Author:	Ashwini	
Purpose: Retains those triples in the section list where the entity is linked to a Wikipedia page	
Input: List of triples	
Output: Triples where the section lists are

---------------------------------------------------

Heuristic: duplicates_removed

Code: /mnt/dell/teknowbase/Code/Relations-Structured/Teknowbase_cleaning/eliminating_dups.py
Author:	Ashwini	
Purpose: Removes duplicates after applying the above filter
Input: List of triples with section lists filtered
Output: All unique triples with section lists filtered


Heuristic: final_lh

Code: /mnt/dell/teknowbase/Code/Relations-Structured/Teknowbase_cleaning/clean_lh/clean_list_hierarchy.py
Author: Ashwini
Purpose: Retains those entities in the list hierarchy that either have a Wikipedia page corresponding to their name, or have a "Main article" associated with them, or do not have any stop words in their names
Input: List of triples with section lists filtered
Output: Triples with clean list hierarchy


Heuristic: toc_final

Code: /mnt/dell/teknowbase/Code/Relations-Structured/Teknowbase_cleaning/clean_toc.py
Author: Ashwini
Purpose: Retains only those triples that contain the exact name of the relation, like "Applications"
Input: List of triples
Output: Cleaned triples with TOC

Heuristic: Re-Extractions

Code: /home/prajna/workspace/prerequsiteStructures/src/preprocess/KeepWikipageNames.java, /mnt/dell/teknowbase/Re-Extractions/map_headings_main_art.py
Author: Prajna, Ashwini
Purpose: Perform re-extractions of the triples for list hierarchy, templates, table of contents and section lists. Added source to the triples extracted from wikipedia list except for the ones involving synonymOf relation. This is because reverse-engineering the source wikipedia page name for "typeOf" relationships is direct, the source is the list page involving the 2nd argument of the triple. However, the same cannot be done for synonymOf relations extracted from wikipedia_list. So, the source has been mentioned as "wikipedia_list" only.

Heuristic: entityNamesSmallCaseUnderscore

Code: /home/prajna/workspace/prerequsiteStructures/src/preprocess/KeepWikipageNames.java
Author: Prajna
Purpose: Change the entity names to lower case and replace spaces by underscore

Heuristic: onlyPageNames

Code: /home/prajna/workspace/prerequsiteStructures/src/preprocess/KeepWikipageNames.java, /mnt/dell/teknowbase/Re-Extractions/map_headings_main_art.py
Author: Prajna, Ashwini
Purpose: For triples extracted using list hierarchy, only those triples where both the entities are valid wikipedia page names or have a "further information" identifier linking to a valid page name or do not have stop words in them have been retained, Section lists triples are filtered by keeping only those where both the arguments are valid wikipedia page names.

Heuristic: postProcessed

Code: /home/prajna/workspace/prerequsiteStructures/src/preprocess/KeepWikipageNames.java
Author: Prajna
Purpose: Done minor postprocessing of the triples where in some cases the entities were surrounded by brackets, there was one or more spaces after the opening bracket, one or more spaces after the closing bracket etc.

Heuristic: badCategoriesRemoved

Code: /home/prajna/workspace/prerequsiteStructures/src/preprocess/KeepWikipageNames.java
Author: Prajna
Purpose: Removed triples where either of the entity belonged to the yago category people, organization, building, yagoGeoEntity, music, location or starts with "list_of".

Heuristic: stemmed

Author: Prajna
Code: /home/prajna/workspace/prerequsiteStructures/src/preprocess/KeepWikipageNames.java
Purpose: Sometimes, the two entities in a triple refer to the same entity in the stemmed form. For example, we had a triple "selection_sort	typeOf	selection_sorts_(algorithms)". We replaced the relation by synonymOf in such cases. Have introduced more synonymOf relations to account for information lost due to removing disambiguation names and stemming. For example, selection_sorts_(algorithm) after removing disambiguation becomes selection_sorts, and after stemming becomes selection_sort. So, we have introduced the triple "selection_sort synonymOf selection_sorts_(algorithm)", "selection_sort synonymOf selection_sort_(algorithm)". This has been added both ways.


